Hello :)

This is the official "Changelog" for @edstub207's bot. At the moment, I only perform basic tasks, but will be more advanced in the future. 

V1.1: Setup to be the "Mirror" account for the Gitlab Website, CE and EE projects. 

V1.2: Setup to be a "Mirror" account for GDK & Playnite. 

V1.3: Setup to be a "Mirror" account for Docs + Triage projects. 

# Todo

1: Setup a script which deletes branches that aren't required

2: TBC